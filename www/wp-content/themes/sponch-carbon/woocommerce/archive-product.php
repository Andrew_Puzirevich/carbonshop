<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header();

?>

    <div class="main-category">
    <h1 class="woocommerce-products-header__title page-title h1-category"><?php woocommerce_page_title(); ?></h1>
<?php


if ( woocommerce_product_loop() ) {

    echo '<div class="cat-block" id="category-page">';
	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();
	echo '</div>';

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {

    ?>
    <div class="center-no-tov">
        <label for="">Скоро в продаже!</label>
        <label for="">Пройдите в каталог и посмотрите <br> другие товары</label>
        <button onclick="document.location.href='/catalog'">Каталог</button>
    </div>
    <?php
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );


?>
    <div class="menuWrapper">
        <div class="menuLimiter">
            <menu class="menu-cat">
                <li class="menu-item">
                    <a class="menu-block-link" id="cases-menu" href="/catalog#cases-menu">чехлы</a>
                </li>
                <li class="menu-item">
                    <a class="menu-block-link" id="hookah-menu" href="/catalog#hookah">кальян</a>
                </li>
                <li class="menu-item">
                    <a class="menu-block-link" id="accessories-menu" href="/catalog#accessories">аксессуары</a>
                </li>
                <li class="menu-item">
                    <a class="menu-block-link" id="exclusive-menu" href="/catalog#exclusive">эксклюзив</a>
                </li>
                <li class="menu-item">
                    <a class="menu-block-link" id="forhome-menu" href="/catalog#forhome">для&nbsp;дома</a>
                </li>
                <li class="menu-item">
                    <a class="menu-block-link" id="safe-menu" href="/catalog#safe">безопасность</a>
                </li>
            </menu>
        </div>
    </div>
    </div>
<?php
get_footer();
