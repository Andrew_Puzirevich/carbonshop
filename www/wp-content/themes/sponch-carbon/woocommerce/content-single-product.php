<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

	<?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
//	do_action( 'woocommerce_before_single_product_summary' );
	?>

    <style>
        .no-img{
            display: none;

        }
        .no-img label{
            position: relative;
            top: 50%;
        }
        .carousel-img{
            display: none;
        }
    </style>
        <div class="single-product">
        <?php the_title( '<h1>', ' <span class="color-in-name"></span></h1>' ); ?>
        </div>

        <div class="product-gallery-block">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 slider-wrap-block">
        <?php
        $post_ids = $product->get_id();
        $tovar = wc_get_product($product->get_id());

        $attachment_ids = $product->get_gallery_attachment_ids();

        ?>

                    <div id="carousel" class="carousel slide" data-interval="0" data-ride="carousel">
                        <div class="carousel-inner gallery-preview">

                                <?php
                                $iter1 = 0;
                                foreach( $attachment_ids as $attachment_id ) {
                                    if ($iter1 == 0){
                                        echo '<div class="item active">';
                                    } else {
                                        echo '<div class="item">';
                                    }

                                    echo wp_get_attachment_image( $attachment_id, array(600,450) );
                                    echo '</div>';
                                    $iter1++;
                                }
                                ?>
                            <div class=" item no-img">
                                <label>Мы пока не успели сделать фото :(</label>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="menuWrapper prod-gallery-wrap">
                            <div class="menuLimiter" >
                                <menu class="menu-cat prod-gallery-carusel">
                                    <?php
                                    $iter2 = 0;
                                    foreach( $attachment_ids as $attachment_id ) {
                                        $idColor = get_post_meta($attachment_id,'color', true);
                                        if ($iter1 == 0){
                                            echo '<li class="menu-item thumb carousel-img active-carousel img-color-'.$idColor.'" id="img-color-'.$idColor.'" data-target="#carousel" data-slide-to='.$iter2.'>';
                                        } else {
                                            echo '<li class="menu-item thumb carousel-img img-color-'.$idColor.'" id="img-color-'.$idColor.'" data-target="#carousel" data-slide-to='.$iter2.'>';
                                        }

                                        echo wp_get_attachment_image( $attachment_id, array(230),0, array('class' => 'menu-block-link') );
                                        echo '</li>';
                                        $iter2++;
                                    }
                                    ?>
                                </menu>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="card_content card_content-product">
            <div class="card_main">
                <div class="card_main_title">Выбери цвет:</div>
                <div class="card_content_squer fix-prod-cart-squer">
                    <a  id="color-2" class="squer two"></a>
                    <a  id="color-1" class="squer"></a>
                    <a  id="color-3" class="squer three"></a>
                    <a  id="color-4" class="squer four"></a>
                    <a  id="color-5" class="squer five"></a>
                    <a  id="color-6" class="squer six"></a>
                    <a  id="color-7" class="squer seven"></a>
                    <a  id="color-8" class="squer eight"></a>
                </div>

            </div>
        </div>
    <div class="tov-price price-prod-cart">
        <label style="font-weight: 400 !important;"><?php echo $tovar->get_price();?> <span>RUB</span></label>
    </div>

    <div class="total">
        <div class="count count-cart">
            <div>
                <button id="btn-minus" class="plus lines">
<!--                    <svg width="21" height="1" viewBox="0 0 21 1" fill="none" xmlns="http://www.w3.org/2000/svg" style="margin: 0 auto;vertical-align: middle;display: table-cell;">-->
<!--                        <line y1="0.5" x2="21" y2="0.5" stroke="black"/>-->
<!--                    </svg>-->
                </button>
            </div>
            <div>
                <label style="font-weight: 400 !important;" id="count-tov">1</label>
            </div>
            <div>
                <button id="btn-plus" class="plus">
<!--                    <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg" style="margin: 0 auto;vertical-align: middle;display: table-cell;">-->
<!--                        <line y1="10.5" x2="21" y2="10.5" stroke="black"/>-->
<!--                        <line x1="10.5" y1="2.0815e-08" x2="10.5" y2="21" stroke="black"/>-->
<!--                    </svg>-->
                </button>
            </div>
        </div>
        <form class="buy-btn" method="post" action="/shopping-cart">
            <input type="hidden" id="selected-color" name="selected-color" value="1">
            <input type="hidden" id="tovar-id" name="tovar-id" value="<?php echo $tovar->get_id(); ?>">
            <input type="hidden" id="col-tov" name="col-tov" value="1">
            <button type="submit" class="material-bubble" name="buy-product-btn">КУПИТЬ</button>
        </form>

    </div>

    <div class="total-btns total-btns-cart">
        <div>
            <button class="continue-shopping material-bubble">СДЕЛАТЬ ИНДИВИДУАЛЬНЫЙ ЗАКАЗ</button>
        </div>
    </div>

    <div class="prod-description">
        <label>О товаре:</label>

        <p>
            <?php
            echo $tovar->get_description();
            ?>
        </p>
    </div>

    <div class="prod-soc-block">
        <label>поделись в соцсетях:</label>
        <div class="prod-soc-block-icons">
            <div><a href="https://www.instagram.com/prime.carbon/" target="_blank"><img src="<?php echo get_template_directory_uri()?>/img/insta.svg" alt=""> instagram</a></div>
            <div><a href="" target="_blank"><img src="<?php echo get_template_directory_uri()?>/img/facebook.svg" alt=""> facebook</a></div>
            <div><a href="" target="_blank"><img src="<?php echo get_template_directory_uri()?>/img/twitter.svg" alt=""> tweet</a></div>
        </div>
    </div>

    <div class="prod-delivery">
        <label>УСЛОВИЯ ДОСТАВКИ:</label>
        <p>Вы можете забрать товар в курьерской службе СДЭК или заказать любое удобное вам отправление. </p><p> Получить заказ можно лично в Екатеринбурге, мы обязательно договоримся о встрече.</p>
    </div>

    <div class="prod-recommendation">
        <label class="prod-recommendation-label">Смотри также</label>
        <div class="recomend-cat">
            <div class="tovar"><a href="/catalog#accessories">
                    <div class="tov-label">
                        <label for="">Кольца</label>
                    </div>
                    <div class="tov-img">
                        <div>
                            <img src="<?php echo get_template_directory_uri()?>/img/mobile/_k 1.svg" alt="">
                        </div>
                    </div>
                    <div class="center-no-tov">
                        <label for="">Скоро в продаже!</label>
                        <label for="">Пройдите в каталог и посмотрите <br> другие товары</label>
                        <button onclick="document.location.href='/catalog'">Каталог</button>
                    </div>
                </a>
            </div>
            <div class="tovar two"><a href="/catalog#hookah">
                    <div class="tov-label">
                        <label for="">Кальяны</label>
                    </div>
                    <div class="tov-img">
                        <div>
                            <img src="<?php echo get_template_directory_uri()?>/img/bong.svg" alt="">
                        </div>
                    </div>
                    <div class="center-no-tov">
                        <label for="">Скоро в продаже!</label>
                        <label for="">Пройдите в каталог и посмотрите <br> другие товары</label>
                        <button onclick="document.location.href='/catalog'">Каталог</button>
                    </div>
                </a>
            </div>
            <div class="tovar three"><a href="/catalog#accessories">
                    <div class="tov-label">
                        <label for="">Запанки</label>
                    </div>
                    <div class="tov-img">
                        <div>
                            <img src="<?php echo get_template_directory_uri()?>/img/acces.svg" alt="">
                        </div>
                    </div>
                    <div class="center-no-tov">
                        <label for="">Скоро в продаже!</label>
                        <label for="">Пройдите в каталог и посмотрите <br> другие товары</label>
                        <button onclick="document.location.href='/catalog'">Каталог</button>
                    </div>
                </a>
            </div>
        </div>
        <div class="menuWrapper">
            <div class="menuLimiter">
                <menu class="menu-cat">
                    <li class="menu-item">
                        <a class="menu-block-link" id="cases-menu" href="/catalog#cases-menu">чехлы</a>
                    </li>
                    <li class="menu-item">
                        <a class="menu-block-link" id="hookah-menu" href="/catalog#hookah">кальян</a>
                    </li>
                    <li class="menu-item">
                        <a class="menu-block-link" id="accessories-menu" href="/catalog#accessories">аксессуары</a>
                    </li>
                    <li class="menu-item">
                        <a class="menu-block-link" id="exclusive-menu" href="/catalog#exclusive">эксклюзив</a>
                    </li>
                    <li class="menu-item">
                        <a class="menu-block-link" id="forhome-menu" href="/catalog#forhome">для&nbsp;дома</a>
                    </li>
                    <li class="menu-item">
                        <a class="menu-block-link" id="safe-menu" href="/catalog#safe">безопасность</a>
                    </li>
                </menu>
            </div>
        </div>
    </div>

    <script>
        "use strict";
        var $ = jQuery;

        $('.menu-item').on('click', function () {
            $('.menu-item').removeClass("active-carousel");
            $(this).addClass("active-carousel");
        });

        $('.squer').on('click', function () {
            var color = $(this).attr('id');
            color = color.split('-');
            $('#selected-color').val(color[1]);



            var findPhotos = $('.img-color-'+color[1]+'').length;
            if (findPhotos > '1'){

                $('.no-img').css("display", "none");
                $('.no-img').removeClass('active');
                $('.prod-gallery-carusel').css("justify-content", "flex-start");
                $('.carousel-img').each(function () {
                    $(this).css("display", "none");
                });
                $('.img-color-'+color[1]+'').each(function () {
                    $(this).css("display", "block");
                });
                if ($('.active').css("display") === 'none'){
                    $('.active').css("display", "");
                }
            }
            if (findPhotos === 1) {

                $('.carousel-img').each(function () {
                    $(this).css("display", "none");
                });
                $('.no-img').css("display", "none");
                $('.no-img').removeClass('active');
                if ($('.active').css("display") === 'none'){
                    $('.active').css("display", "");
                }
            }
            if (findPhotos === 0){
                // $('.item').each(function () {
                //     $(this).removeClass('active');
                // });
                $('.active').css("display", "none");
                $('.carousel-img').each(function () {
                    $(this).css("display", "none");
                });
                $('.no-img').addClass('active');
                $('.no-img').css("display", "block");

            }
        });

        var countTovar = $('#count-tov').text();

        $('#btn-minus').on('click', function () {
            if (countTovar > 1){
                countTovar--;
                $('#count-tov').text(countTovar);
                $('#col-tov').val(countTovar);
            }
        });
        $('#btn-plus').on('click', function () {
            countTovar++;
            $('#count-tov').text(countTovar);
            $('#col-tov').val(countTovar);
        });

        var squer = $('.squer');

        squer.on('click', function () {
            squer.removeClass('is-active');
            $(this).addClass('is-active');
            var idColor = $(this).attr("id");
            idColor = idColor.split('-');
            if ($(this).is('#color-'+idColor[1]+'')) {
                $('.menu-item').removeClass('active-carousel');
                $('#img-color-'+idColor[1]+'').click();
            }

            var nameColor = '';
            switch ($('#selected-color').val()) {
                case '1':
                    nameColor = 'Emerald green';
                    break;
                case '2':
                    nameColor = 'Clearcoat';
                    break;
                case '3':
                    nameColor = 'Clearcoat Matt';
                    break;
                case '4':
                    nameColor = 'Blue jewel';
                    break;
                case '5':
                    nameColor = 'Sapphire blue';
                    break;
                case '6':
                    nameColor = 'Red apple';
                    break;
                case '7':
                    nameColor = 'Navy blue';
                    break;
                case '8':
                    nameColor = 'Condensed copper';
                    break;
                default:
                    nameColor = '';
            }
            $('.color-in-name').html(nameColor);
        });

        $(document).on('ready', function () {
            $('#color-2').click();
        })
    </script>

</div>
