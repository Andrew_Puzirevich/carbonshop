<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;
?>
<style>
    .color-div{
        margin: 40px 0;
        position: relative;
    }
</style>
<label class="order-list">Состав заказа</label>
<?php
foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ):
    $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
    $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

    if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ):
        $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );

    $prod_str = WC()->cart->get_cart_hash().'+'. $product_id.'+'.$cart_item['color'].'+'.$cart_item['quantity'];

        ?>
        <div class="order-list-product">
            <input type="hidden" class="save-color" value='<?php echo $prod_str;?>'>
            <div class="prod-photo-check">
                <?php
                $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image('150'), $cart_item, $cart_item_key );

                if ( ! $product_permalink ) {
                    echo $thumbnail; // PHPCS: XSS ok.
                } else {
                    printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
                }
                ?>
            </div>
            <div class="prod-info-check">
                <div class="prod-info-check-name">
                    <label>
                        <?php
                        if ( ! $product_permalink ) {
                            echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
                        } else {
                            echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
                        }

                        do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

                        // Meta data.
                        echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

                        // Backorder notification.
                        if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                            echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
                        }
                        ?>
                    </label>
                </div>
                <?php
                $color = $cart_item['color'];
                $classColor = '';
                switch ($color):
                    case 2:
                        $classColor = 'two';
                        break;
                    case 3:
                        $classColor = 'three';
                        break;
                    case 4:
                        $classColor = 'four';
                        break;
                    case 5:
                        $classColor = 'five';
                        break;
                    case 6:
                        $classColor = 'six';
                        break;
                    case 7:
                        $classColor = 'seven';
                        break;
                    case 8:
                        $classColor = 'eight';
                        break;
                    default:
                        $classColor = '';
                        break;
                endswitch;
                ?>

                <div class="color-div">
                    <label>Цвет:</label><div class="squer <?php echo $classColor; ?>"></div>
                </div>
                <div class="prod-check-details">
                    <div>
                        <?php echo $cart_item['quantity']; ?> шт
                    </div>
                    <div>
                        <label><?php
                            $total =  apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
                            $total = preg_replace( '/[^.\d]/', '', $total  );
                            echo $total;
                            ?> <span>RUB</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    <?php
    endif;
endforeach;
?>

<div class="checkout-total">
    <label>Итого к оплате:</label>
    <div class="checkout-total-price">
        <label><?php $sub_total = WC()->cart->get_cart_subtotal(); $sub_total = preg_replace( '/[^.\d]/', '', $sub_total  ); echo $sub_total;?> <span>RUB</span></label>
    </div>
</div>
