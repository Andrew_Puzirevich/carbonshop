<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

?>
<div class="tovar">
    <a href="/product/<?php echo $product->slug?>/">
        <?php
        $new = get_post_meta($product->id, 'isNew', true);
        if ($new == 'true'):
        ?>
        <div class="tovar-label__new">новинка</div>
        <?php endif; ?>
        <div class="tov-label">
            <label for=""><?php echo $product->name?></label>
        </div>
        <div class="tov-img">
            <div>
                <?php
                echo woocommerce_get_product_thumbnail('150');
                ?>
            </div>
        </div>
        <div class="tov-price">
            <label><?php echo $product->price; ?> <span>RUB</span></label>
        </div>
    </a>
</div>